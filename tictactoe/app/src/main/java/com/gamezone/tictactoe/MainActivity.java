package com.gamezone.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static String EXTRA_PLAYER1_NAME = "tictactoe.player1_name";
    public static String EXTRA_PLAYER2_NAME = "tictactoe.player2_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setSupportActionBar((androidx.appcompat.widget.Toolbar)findViewById(R.id.toolbar));
    }

    public void startGame(View view) {
        EditText editTextPlayer1 = (EditText)findViewById(R.id.editTextPlayer1);
        EditText editTextPlayer2 = (EditText)findViewById(R.id.editTextPlayer2);
        String player1 = editTextPlayer1.getText().toString();
        String player2 = editTextPlayer2.getText().toString();

        if (player1.isEmpty() || player2.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter the player names!", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(EXTRA_PLAYER1_NAME, player1);
        intent.putExtra(EXTRA_PLAYER2_NAME, player2);
        startActivity(intent);
    }
}
