package com.gamezone.tictactoe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chiralcode.colorpicker.ColorPickerDialog;

import java.util.Random;

public class GameActivity extends AppCompatActivity {

    private enum Turn {
        TURN_X,
        TURN_0
    }

    private Turn currentTurn = Turn.TURN_X;
    private String currentPlayerName;
    private boolean finished = false;
    private String player1Name;
    private String player2Name;

    private void newGame() {
        int rand = new Random().nextInt(2);

        TextView status = findViewById(R.id.statusText);

        if (rand == 0) {
            status.setText("Current turn: " + player1Name);
            currentPlayerName = player1Name;
        } else {
            status.setText("Current turn: " + player2Name);
            currentPlayerName = player2Name;
        }

        currentTurn = Turn.TURN_X;
        finished = false;

        ((Button)findViewById(R.id.button1)).setText("");
        ((Button)findViewById(R.id.button2)).setText("");
        ((Button)findViewById(R.id.button3)).setText("");
        ((Button)findViewById(R.id.button4)).setText("");
        ((Button)findViewById(R.id.button5)).setText("");
        ((Button)findViewById(R.id.button6)).setText("");
        ((Button)findViewById(R.id.button7)).setText("");
        ((Button)findViewById(R.id.button8)).setText("");
        ((Button)findViewById(R.id.button9)).setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setTitle("My Toolbar");
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        player1Name = intent.getStringExtra(MainActivity.EXTRA_PLAYER1_NAME);
        player2Name = intent.getStringExtra(MainActivity.EXTRA_PLAYER2_NAME);

        newGame();
    }

    public void clickButton(View view) {
        if (finished) {
            return;
        }

        Button button = (Button)view;

        if (!button.getText().toString().isEmpty()) {
            return;
        }

        TextView status = findViewById(R.id.statusText);

        if (currentTurn == Turn.TURN_X) {
            button.setText("X");
            currentTurn = Turn.TURN_0;
        } else {
            button.setText("0");
            currentTurn = Turn.TURN_X;
        }

        String s1 = ((Button)findViewById(R.id.button1)).getText().toString();
        String s2 = ((Button)findViewById(R.id.button2)).getText().toString();
        String s3 = ((Button)findViewById(R.id.button3)).getText().toString();
        String s4 = ((Button)findViewById(R.id.button4)).getText().toString();
        String s5 = ((Button)findViewById(R.id.button5)).getText().toString();
        String s6 = ((Button)findViewById(R.id.button6)).getText().toString();
        String s7 = ((Button)findViewById(R.id.button7)).getText().toString();
        String s8 = ((Button)findViewById(R.id.button8)).getText().toString();
        String s9 = ((Button)findViewById(R.id.button9)).getText().toString();

        if ((s1.equals("X") && s1.equals(s2) && s1.equals(s3)) ||
            (s4.equals("X") && s4.equals(s5) && s4.equals(s6)) ||
            (s7.equals("X") && s7.equals(s8) && s7.equals(s9)) ||
            (s1.equals("X") && s1.equals(s4) && s1.equals(s7)) ||
            (s2.equals("X") && s2.equals(s5) && s2.equals(s8)) ||
            (s3.equals("X") && s3.equals(s6) && s3.equals(s9)) ||
            (s1.equals("X") && s1.equals(s5) && s1.equals(s9)) ||
            (s3.equals("X") && s3.equals(s5) && s3.equals(s7)))
        {
            status.setText("Winner: " + currentPlayerName);
            finished = true;
        }
        else if ((s1.equals("0") && s1.equals(s2) && s1.equals(s3)) ||
                (s4.equals("0") && s4.equals(s5) && s4.equals(s6)) ||
                (s7.equals("0") && s7.equals(s8) && s7.equals(s9)) ||
                (s1.equals("0") && s1.equals(s4) && s1.equals(s7)) ||
                (s2.equals("0") && s2.equals(s5) && s2.equals(s8)) ||
                (s3.equals("0") && s3.equals(s6) && s3.equals(s9)) ||
                (s1.equals("0") && s1.equals(s5) && s1.equals(s9)) ||
                (s3.equals("0") && s3.equals(s5) && s3.equals(s7)))
        {
            status.setText("Winner: " + currentPlayerName);
            finished = true;
        }
        else if (!s1.isEmpty() && !s2.isEmpty() && !s3.isEmpty() &&
                !s4.isEmpty() && !s5.isEmpty() && !s6.isEmpty() &&
                !s7.isEmpty() && !s8.isEmpty() && !s9.isEmpty())
        {
            status.setText("It's a draw");
            finished = true;
        }

        if (!finished) {
            if (currentPlayerName.equals(player1Name)) {
                currentPlayerName = player2Name;
            } else {
                currentPlayerName = player1Name;
            }
            status.setText("Current turn: " + currentPlayerName);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_color) {
            ColorPickerDialog colorPickerDialog = new ColorPickerDialog(this, Color.WHITE, new ColorPickerDialog.OnColorSelectedListener() {
                @Override
                public void onColorSelected(int color) {
                    ConstraintLayout layout = findViewById(R.id.layout);
                    layout.setBackgroundColor(color);
                }
            });
            colorPickerDialog.show();
        } else if (id == R.id.action_reset) {
            newGame();
        }

        return super.onOptionsItemSelected(item);
    }
}
