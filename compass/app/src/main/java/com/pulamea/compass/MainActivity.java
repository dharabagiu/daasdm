package com.pulamea.compass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

public class MainActivity extends AppCompatActivity {

    SensorManager sensorManager;
    Sensor accelerometerSensor;
    Sensor magneticSensor;
    SensorEventListener accelerometerListener;
    SensorEventListener magneticListener;
    float[] gravity;
    float[] geomagnetic;
    float lastRotation = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gravity = new float[]{ 0.0f, 0.0f, 0.0f };
        geomagnetic = new float[]{ 0.0f, 0.0f, 0.0f };
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accelerometerListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                gravity = lowPass(event.values.clone(), gravity);
                updateCompassRotation();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        magneticListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                geomagnetic = lowPass(event.values.clone(), geomagnetic);
                updateCompassRotation();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(accelerometerListener, accelerometerSensor, Sensor.TYPE_ACCELEROMETER,
                SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(magneticListener, magneticSensor, Sensor.TYPE_MAGNETIC_FIELD,
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(accelerometerListener);
        sensorManager.unregisterListener(magneticListener);
    }

    private void updateCompassRotation() {
        if (gravity == null || geomagnetic == null) {
            return;
        }

        float[] rotationMatrix = new float[9];
        float[] orientation = new float[3];

        SensorManager.getRotationMatrix(rotationMatrix, null, gravity, geomagnetic);
        SensorManager.getOrientation(rotationMatrix, orientation);

        float newRotation = (float)(orientation[0] * 180 / Math.PI);
        if (newRotation < 0.0f)
        {
            newRotation += 360.0f;
        }
        newRotation = -newRotation;

        Animation animation = new RotateAnimation(lastRotation, newRotation,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(100);
        animation.setFillAfter(true);

        findViewById(R.id.image_compass).startAnimation(animation);

        lastRotation = newRotation;
    }

    private static float[] lowPass(float[] input, float[] output) {
        if ( output == null) {
            return input;
        }
        for (int i = 0; i < input.length; ++i) {
            output[i] = output[i] + 0.15f * (input[i] - output[i]);
        }
        return output;
    }
}
