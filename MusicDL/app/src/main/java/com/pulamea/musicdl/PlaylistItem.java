package com.pulamea.musicdl;

public class PlaylistItem {

    public String songName;
    public boolean downloaded;
    public int progress;
    public boolean downloading;

    public PlaylistItem(String songName, boolean downloaded) {
        this.songName = songName;
        this.downloaded = downloaded;
        this.progress = 0;
        this.downloading = false;
    }

}
