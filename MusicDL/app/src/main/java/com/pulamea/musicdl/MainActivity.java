package com.pulamea.musicdl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements DownloadReceiverListener {

    private ArrayList<PlaylistItem> playlist;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra(Extras.EXTRA_DOWNLOAD_URL, DownloadService.API_DOWNLOAD_PLAYLIST_URL);
        intent.putExtra(Extras.EXTRA_DOWNLOAD_RECEIVER, new DownloadReceiver(new Handler(), this));
        startService(intent);
    }

    @Override
    public void updatePlaylist(String playlistText) {
        File musicDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC), "music");
        if (!musicDirectory.exists()) {
            musicDirectory.mkdirs();
        }
        String[] songs = playlistText.split("\\r?\\n|\\r");
        playlist = new ArrayList<>();
        for (String song : songs) {
            File musicFile = new File(musicDirectory, song);
            playlist.add(new PlaylistItem(song, musicFile.exists()));
        }
        refreshPlaylist();
    }

    @Override
    public void updateProgress(String songName, int progress) {
        for (PlaylistItem song : playlist) {
            if (song.songName.equals(songName)) {
                song.progress = progress;
                break;
            }
        }
        refreshPlaylist();
    }

    @Override
    public void downloadFinished(String songName) {
        for (PlaylistItem song : playlist) {
            if (song.songName.equals(songName)) {
                song.progress = 0;
                song.downloading = false;
                song.downloaded = true;
                break;
            }
        }
        refreshPlaylist();
    }

    private void refreshPlaylist() {
        PlaylistAdapter adapter = new PlaylistAdapter(this, playlist);
        ListView listView = findViewById(R.id.playlist);
        listView.setAdapter(adapter);
    }

    public void playSong(String songName) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        File musicDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC), "music");
        mediaPlayer = MediaPlayer.create(this, Uri.parse(new File(musicDirectory, songName).getAbsolutePath()));
        mediaPlayer.start();
    }
}
