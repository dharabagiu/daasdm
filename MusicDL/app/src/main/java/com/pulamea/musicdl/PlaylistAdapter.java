package com.pulamea.musicdl;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PlaylistAdapter extends ArrayAdapter<PlaylistItem> {

    List<PlaylistItem> playlist;

    public PlaylistAdapter(Context context, ArrayList<PlaylistItem> playlist) {
        super(context, R.layout.playlist_item, playlist);
        this.playlist = playlist;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.playlist_item, parent, false);
        }
        TextView textSongName = convertView.findViewById(R.id.text_song_name);
        textSongName.setText(playlist.get(position).songName);

        ProgressBar progressBar = convertView.findViewById(R.id.progress_bar);
        ImageView imageAction = convertView.findViewById(R.id.image_action);
        final View finalConvertView = convertView;

        if (!playlist.get(position).downloaded) {
            imageAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProgressBar progressBar = finalConvertView.findViewById(R.id.progress_bar);
                    ImageView imageAction = finalConvertView.findViewById(R.id.image_action);
                    progressBar.setVisibility(View.INVISIBLE);
                    progressBar.setProgress(0);
                    playlist.get(position).downloading = true;
                    imageAction.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(getContext(), DownloadService.class);
                    intent.putExtra(Extras.EXTRA_DOWNLOAD_URL, DownloadService.API_DOWNLOAD_MUSIC_URL + "?basename=" + playlist.get(position).songName);
                    intent.putExtra(Extras.EXTRA_DOWNLOAD_RECEIVER, new DownloadReceiver(new Handler(), (MainActivity) getContext()));
                    getContext().startService(intent);
                }
            });
        } else {
            imageAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity)getContext()).playSong(playlist.get(position).songName);
                }
            });
        }

        if (playlist.get(position).downloading) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(playlist.get(position).progress);
            imageAction.setVisibility(View.INVISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            imageAction.setVisibility(View.VISIBLE);
            if (playlist.get(position).downloaded) {
                imageAction.setImageDrawable(getContext().getDrawable(R.drawable.ic_music_note_black_24dp));
            }
        }

        return convertView;
    }

}
