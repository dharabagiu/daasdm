package com.pulamea.musicdl;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public class DownloadService extends IntentService {

    public static final int UPDATE_PROGRESS = 1000;
    public static final int DOWNLOAD_FINISHED = 3000;
    public static final int PLAYLIST_READY = 2000;
    public static final String BUNDLE_PLAYLIST_DATA = "com.pulamea.musicdl.DownloadService.PLAYLIST_DATA";
    public static final String BUNDLE_SONG_NAME = "com.pulamea.musicdl.DownloadService.SONG_NAME";
    public static final String BUNDLE_DOWNLOAD_PROGRESS = "com.pulamea.musicdl.DownloadService.DOWNLOAD_PROGRESS";
    public static final String SERVER_URL = "http://10.0.2.2:8080/";
    public static final String API_DOWNLOAD_PLAYLIST_URL = "downloadPlaylist";
    public static final String API_DOWNLOAD_MUSIC_URL = "downloadMusic";

    private ResultReceiver receiver;

    public DownloadService() {
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String urlToDownload = intent.getStringExtra(Extras.EXTRA_DOWNLOAD_URL);
        receiver = (ResultReceiver)intent.getParcelableExtra(Extras.EXTRA_DOWNLOAD_RECEIVER);
        if (urlToDownload.contains(API_DOWNLOAD_PLAYLIST_URL)) {
            downloadPlaylist(SERVER_URL + API_DOWNLOAD_PLAYLIST_URL);
        } else if (urlToDownload.contains(API_DOWNLOAD_MUSIC_URL)) {
            downloadMusic(SERVER_URL + urlToDownload);
        }
    }

    private void downloadPlaylist(String url) {
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection)_url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String playlist = "";
            byte[] buffer = new byte[4096];
            int len;
            while ((len = in.read(buffer)) != -1) {
                playlist += new String(Arrays.copyOfRange(buffer, 0, len));
            }
            Bundle playlistData = new Bundle();
            playlistData.putString(BUNDLE_PLAYLIST_DATA, playlist);
            receiver.send(PLAYLIST_READY, playlistData);
            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void downloadMusic(String url) {
        File musicDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC), "music");
        if (!musicDirectory.exists()) {
            musicDirectory.mkdirs();
        }
        String songName = url.substring(url.indexOf('=') + 1);
        File musicFile = new File(musicDirectory, songName);
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection)_url.openConnection();
            int sizeOfFile = urlConnection.getContentLength();
            int bytesDownloaded = 0;
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            FileOutputStream out = new FileOutputStream(musicFile);
            byte[] buffer = new byte[4096];
            int len;
            while ((len = in.read(buffer)) != -1) {
                bytesDownloaded += len;
                Bundle downloadProgress = new Bundle();
                downloadProgress.putString(BUNDLE_SONG_NAME, songName);
                downloadProgress.putInt(BUNDLE_DOWNLOAD_PROGRESS, (int)((double)bytesDownloaded / sizeOfFile * 100));
                receiver.send(UPDATE_PROGRESS, downloadProgress);
                out.write(buffer, 0, len);
            }
            out.close();
            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bundle downloadFinishedData = new Bundle();
        downloadFinishedData.putString(BUNDLE_SONG_NAME, songName);
        receiver.send(DOWNLOAD_FINISHED, downloadFinishedData);
    }

}
