package com.pulamea.musicdl;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DownloadReceiver extends ResultReceiver {

    private DownloadReceiverListener listener;

    public DownloadReceiver(Handler handler, DownloadReceiverListener listener) {
        super(handler);
        this.listener = listener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);

        if (resultCode == DownloadService.UPDATE_PROGRESS) {
            int downloadProgress = resultData.getInt(DownloadService.BUNDLE_DOWNLOAD_PROGRESS);
            String songName = resultData.getString(DownloadService.BUNDLE_SONG_NAME);
            listener.updateProgress(songName, downloadProgress);
        } else if (resultCode == DownloadService.PLAYLIST_READY) {
            String playlist = resultData.getString(DownloadService.BUNDLE_PLAYLIST_DATA);
            listener.updatePlaylist(playlist);
        } else if (resultCode == DownloadService.DOWNLOAD_FINISHED) {
            String songName = resultData.getString(DownloadService.BUNDLE_SONG_NAME);
            listener.downloadFinished(songName);
        }
    }

}
