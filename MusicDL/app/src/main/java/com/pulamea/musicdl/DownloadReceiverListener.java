package com.pulamea.musicdl;

public interface DownloadReceiverListener {

    void updatePlaylist(String playlist);
    void updateProgress(String songName, int progress);
    void downloadFinished(String songName);

}
