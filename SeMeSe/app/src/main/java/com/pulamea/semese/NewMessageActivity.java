package com.pulamea.semese;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class NewMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        if (intent.hasExtra(Extras.EXTRA_PHONE_NUMBER)) {
            TextView to = findViewById(R.id.edit_receiver);
            to.setText(intent.getStringExtra(Extras.EXTRA_PHONE_NUMBER));
        }
    }

    public void sendSms(View view) {
        String to = ((EditText)findViewById(R.id.edit_receiver)).getText().toString();
        String message = ((EditText)findViewById(R.id.edit_message)).getText().toString();

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(to, null, message, null, null);

        SmsDB db = new SmsDB(this);
        db.insertSms(new Sms(to, message, Sms.SmsType.OUTGOING));

        ((EditText)findViewById(R.id.edit_message)).setText("");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
