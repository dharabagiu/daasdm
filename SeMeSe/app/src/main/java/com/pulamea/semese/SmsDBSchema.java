package com.pulamea.semese;

import android.provider.BaseColumns;

public final class SmsDBSchema {

    private SmsDBSchema() {

    }

    public static class SmsTable implements BaseColumns {

        public static final String TABLE_NAME = "sms";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";

    }

}
