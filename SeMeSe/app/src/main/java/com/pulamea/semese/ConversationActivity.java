package com.pulamea.semese;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ListView;

import java.util.List;

public class ConversationActivity extends AppCompatActivity {

    private static ConversationActivity activeInstance = null;

    private String phoneNumber;
    private List<Sms> smss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        phoneNumber = getIntent().getStringExtra(Extras.EXTRA_PHONE_NUMBER);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(phoneNumber);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ConversationActivity.this,
                        NewMessageActivity.class);
                intent.putExtra(Extras.EXTRA_PHONE_NUMBER, phoneNumber);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        activeInstance = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        activeInstance = this;
        refreshSmsList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void refreshSmsList() {
        SmsDB smsDB = new SmsDB(getApplicationContext());
        smss = smsDB.getConversation(phoneNumber);

        ListView smsListView = findViewById(R.id.list_sms);
        MessagesList smsAdapter = new MessagesList(getApplicationContext(), smss);
        smsListView.setAdapter(smsAdapter);
    }

    public static void refreshActiveInstance() {
        if (activeInstance != null) {
            activeInstance.refreshSmsList();
        }
    }

}
