package com.pulamea.semese;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Timestamp {

    private long value;

    public Timestamp(long value) {
        this.value = value;
    }

    public Timestamp() {
        this.value = Instant.now().toEpochMilli();
    }

    public long getValue() {
        return value;
    }

    public String getShortDate() {
        Instant instant = Instant.ofEpochMilli(value);
        return DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.getDefault())
                .withZone(ZoneId.systemDefault()).format(instant);
    }

    public String getLongDate() {
        Instant instant = Instant.ofEpochMilli(value);
        return DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").withLocale(Locale.getDefault()).
                withZone(ZoneId.systemDefault()).format(instant);
    }

}
