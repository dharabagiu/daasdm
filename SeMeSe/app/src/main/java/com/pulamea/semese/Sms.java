package com.pulamea.semese;

public class Sms {

    public enum SmsType {
        INCOMING,
        OUTGOING
    }

    private String phone;
    private String message;
    private SmsType type;
    private Timestamp timestamp;

    public Sms(String phone, String message, SmsType type, Timestamp timestamp) {
        this.phone = phone;
        this.message = message;
        this.type = type;
        this.timestamp = timestamp;
    }

    public Sms(String phone, String message, SmsType type) {
        this(phone, message, type, new Timestamp());
    }

    public String getPhone() {
        return phone;
    }

    public String getMessage() {
        return message;
    }

    public SmsType getType() {
        return type;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

}
