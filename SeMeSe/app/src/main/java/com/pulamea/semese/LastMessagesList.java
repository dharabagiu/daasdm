package com.pulamea.semese;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ArrayAdapter;

import androidx.core.content.ContextCompat;

import java.util.List;

public class LastMessagesList extends ArrayAdapter<Sms> {

    private static class ViewHolder {
        TextView textTime;
        TextView textPhone;
        TextView textMessage;
    }

    public LastMessagesList(Context context, List<Sms> smss) {
        super(context, R.layout.last_messages_element, smss);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Sms sms = getItem(position);
        ViewHolder viewHolder;

        View result;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.last_messages_element, parent, false);

            viewHolder.textTime = convertView.findViewById(R.id.text_time);
            viewHolder.textPhone = convertView.findViewById(R.id.text_phone);
            viewHolder.textMessage = convertView.findViewById(R.id.text_message);

            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
            result = convertView;
        }

        viewHolder.textTime.setText(sms.getTimestamp().getShortDate());
        viewHolder.textPhone.setText(sms.getPhone());
        viewHolder.textMessage.setText(sms.getMessage());

        return convertView;
    }
}
