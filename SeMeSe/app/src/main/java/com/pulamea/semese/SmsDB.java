package com.pulamea.semese;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class SmsDB {

    public static final String SQL_GET_LAST_MESSAGES = "SELECT phone, message, type, timestamp " +
            "FROM sms s1 WHERE timestamp = (SELECT MAX(timestamp) FROM sms s2 WHERE s2.phone = s1.phone)";

    public static final String SQL_GET_CONVERSATION = "SELECT phone, message, type, timestamp " +
            "FROM sms WHERE phone = ?";

    private SmsDBHelper helper;

    public SmsDB(Context context) {
        helper = new SmsDBHelper(context);
    }

    public long insertSms(Sms sms)
    {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SmsDBSchema.SmsTable.COLUMN_NAME_PHONE, sms.getPhone());
        values.put(SmsDBSchema.SmsTable.COLUMN_NAME_MESSAGE, sms.getMessage());
        values.put(SmsDBSchema.SmsTable.COLUMN_NAME_TYPE,
                sms.getType() == Sms.SmsType.INCOMING ? 1 : 2);
        values.put(SmsDBSchema.SmsTable.COLUMN_NAME_TIMESTAMP, sms.getTimestamp().getValue());
        long id = db.insert(SmsDBSchema.SmsTable.TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public List<Sms> getLastSmss() {
        return getSmss(SQL_GET_LAST_MESSAGES, null);
    }

    public List<Sms> getConversation(String phone) {
        return getSmss(SQL_GET_CONVERSATION, new String[] {phone});
    }

    private List<Sms> getSmss(String query, String[] selectionArgs) {
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, selectionArgs);

        List<Sms> smss = new ArrayList<Sms>();

        if (cursor.moveToFirst()) {
            do {
                smss.add(new Sms(cursor.getString(0), cursor.getString(1),
                        cursor.getInt(2) == 1 ? Sms.SmsType.INCOMING : Sms.SmsType.OUTGOING,
                        new Timestamp(cursor.getLong(3))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return smss;
    }

}
