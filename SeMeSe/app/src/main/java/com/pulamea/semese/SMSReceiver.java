package com.pulamea.semese;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;

public class SMSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            SmsMessage[] smss = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            SmsDB db = new SmsDB(context);
            for (SmsMessage sms : smss) {
                db.insertSms(new Sms(sms.getOriginatingAddress(), sms.getMessageBody(),
                        Sms.SmsType.INCOMING));
            }
            MainActivity.refreshActiveInstance();
            ConversationActivity.refreshActiveInstance();
        }
    }

}
