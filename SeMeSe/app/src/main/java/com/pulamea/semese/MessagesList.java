package com.pulamea.semese;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.List;

public class MessagesList extends ArrayAdapter<Sms> {

    private static class ViewHolder {
        TextView textTime;
        TextView textMessage;
    }

    public MessagesList(Context context, List<Sms> smss) {
        super(context, R.layout.messages_element, smss);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Sms sms = getItem(position);
        MessagesList.ViewHolder viewHolder;

        View result;
        if (convertView == null) {
            viewHolder = new MessagesList.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.messages_element, parent, false);

            viewHolder.textTime = convertView.findViewById(R.id.text_time);
            viewHolder.textMessage = convertView.findViewById(R.id.text_message);

            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MessagesList.ViewHolder)convertView.getTag();
            result = convertView;
        }

        if (sms.getType() == Sms.SmsType.INCOMING) {
            convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.messageReceived));
        } else {
            convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.messageSent));
        }

        viewHolder.textTime.setText(sms.getTimestamp().getLongDate());
        viewHolder.textMessage.setText(sms.getMessage());

        return convertView;
    }

}
