package com.pulamea.semese;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static MainActivity activeInstance = null;
    private List<Sms> smss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        NewMessageActivity.class);
                startActivity(intent);
            }
        });

        ListView smsListView = findViewById(R.id.last_messages_list);
        smsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Sms sms = smss.get(position);
                Intent intent = new Intent(MainActivity.this,
                        ConversationActivity.class);
                intent.putExtra(Extras.EXTRA_PHONE_NUMBER, sms.getPhone());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        activeInstance = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        refreshSmsList();

        activeInstance = this;
    }

    private void refreshSmsList() {
        SmsDB smsDB = new SmsDB(getApplicationContext());
        smss = smsDB.getLastSmss();

        ListView smsListView = findViewById(R.id.last_messages_list);
        LastMessagesList smsAdapter = new LastMessagesList(getApplicationContext(), smss);
        smsListView.setAdapter(smsAdapter);
    }

    public static void refreshActiveInstance() {
        if (activeInstance != null) {
            activeInstance.refreshSmsList();
        }
    }

}
