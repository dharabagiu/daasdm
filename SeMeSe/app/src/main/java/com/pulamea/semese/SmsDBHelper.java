package com.pulamea.semese;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SmsDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "sms.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + SmsDBSchema.SmsTable.TABLE_NAME + "(" +
            SmsDBSchema.SmsTable._ID + " INTEGER PRIMARY KEY," +
            SmsDBSchema.SmsTable.COLUMN_NAME_PHONE + " TEXT," +
            SmsDBSchema.SmsTable.COLUMN_NAME_MESSAGE + " TEXT," +
            SmsDBSchema.SmsTable.COLUMN_NAME_TYPE + " INTEGER," +
            SmsDBSchema.SmsTable.COLUMN_NAME_TIMESTAMP + " INTEGER)";

    public SmsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
