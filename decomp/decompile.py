#!/usr/bin/env python

import os, sys, argparse
from androguard.core.bytecodes import apk
from androguard.core.bytecodes import dvm
from androguard.core.analysis import analysis
from androguard.decompiler.dad import decompile

def getClassPath(className):
    # va returna calea unde va trebui salvata o anumita clasa
    # get_name() pentru un obiect ClassDefItem va returna un nume de clasa care 
    # va incepe cu "L" si se termina cu ";" ex: Ljava/lang/Object;
    return '%s.java' % className[1:-1]

def getClassName(className):
    # va returna numele unei clase
    # ex. pentru Ljava/lang/Object; va returna: java.lang.Object
    return className[1:-1].replace('/', '.')
    
def makeDirs(directory):
    # va crea un director cu toate subdirectoarele care inca nu exista
    if not os.path.isdir(directory):
        os.makedirs(directory)
                
def decompileMethod(methodObj, analysis):
    # <methodObj> e un obiect de tipul EncodedMethod
    # <analysis> e un obiect de tipul VMAnalysis
    if methodObj.get_code() == None:
        return None
    methodAnalysis = analysis.get_method(methodObj)    # returns MethodAnalysis object
    decompMethod = decompile.DvMethod(methodAnalysis)
    try:
        decompMethod.process()
        methodSource = decompMethod.get_source()
    except:
        print('Failed to decompile [%s]' % methodObj.get_name())
        return '''   method %s() {
        // failed to decompile
    }'''
    return methodSource
    
parser = argparse.ArgumentParser(description='Decompiler for APKs')

parser.add_argument('apkpath', help='path to apk')
parser.add_argument('-manifest', action='store_true', help='save AndroidManifest.xml')
parser.add_argument('-perms', action='store_true', help='list permissions')
parser.add_argument('-activities', action='store_true', help='list activities')
parser.add_argument('-decomp', action='store_true', help='decompile apk')

args = parser.parse_args()

apkpath = args.apkpath
apkobj = apk.APK(apkpath)

if args.manifest:
    dirname = os.path.splitext(apkpath)[0]
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
    file = open(os.path.join(dirname, 'AndroidManifest.xml'), 'wb')
    file.write(apkobj.get_android_manifest_axml().get_xml())
    file.close()
    
if args.perms:
    perms = apkobj.get_permissions()
    for p in perms:
        print(p)
    
if args.activities:
    activities = apkobj.get_activities()
    for a in activities:
        print(a)
    
if args.decomp:
    rawdex = apkobj.get_dex()
    dex = dvm.DalvikVMFormat(rawdex, decompiler='dad')
    analysis = analysis.Analysis(dex)
    dirname = os.path.splitext(apkpath)[0]
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
    for current_class in dex.get_classes():
        # Get class name and path
        classDir = os.path.dirname(os.path.join(dirname, getClassPath(current_class.get_name())))
        makeDirs(classDir)
        classPath = os.path.join(dirname, getClassPath(current_class.get_name()))
        className = getClassName(current_class.get_name())

        # Open java file
        file = open(classPath, 'w')

        # Write class name
        file.write('class %s ' % className)

        # Write interfaces
        interfaces = current_class.get_interfaces()
        if len(interfaces) != 0:
            file.write('implements ')
        for i in range(0, len(interfaces)):
            file.write(getClassName(interfaces[i]))
            if i != len(interfaces) - 1:
                file.write(',')
            file.write(' ')

        # Write super class
        superClass = current_class.get_superclassname()
        if superClass:
            file.write('extends %s ' % getClassName(superClass))

        file.write('{\n')
        
        # Write fields
        for field in current_class.get_fields():
            access_flags = field.get_access_flags_string()
            if access_flags == "0x0":
                access_flags = ""
            file.write("\n\t%s %s %s;" % (access_flags, getClassName(field.get_descriptor()), field.get_name()))
        
        if len(current_class.get_fields()) != 0:
            file.write('\n')
        
        # Write methods
        for method in current_class.get_methods():
            methodSource = decompileMethod(method, analysis)
            file.write(str(methodSource))

        # Wrap up
        file.write('}\n')
        file.close()
