package com.pulamea.hotels;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class HotelsDB {

    private HotelsDBHelper helper;

    public HotelsDB(Context context) {
        helper = new HotelsDBHelper(context);
    }

    public long insertHotel(Hotel hotel) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_NAME, hotel.getName());
        values.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_ADDRESS, hotel.getAddress());
        values.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_PHONE, hotel.getPhone());
        values.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_WEBPAGE, hotel.getWebpage());
        long id = db.insert(HotelsDBSchema.HotelsTable.TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public List<Hotel> getHotels() {
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] columns = { HotelsDBSchema.HotelsTable.COLUMN_NAME_NAME,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_ADDRESS,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_PHONE,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_WEBPAGE};
        Cursor cursor = db.query(HotelsDBSchema.HotelsTable.TABLE_NAME, columns, null,
                null, null, null,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_NAME);

        List<Hotel> hotels = new ArrayList<Hotel>();

        if (cursor.moveToFirst()) {
            do {
                hotels.add(new Hotel(cursor.getString(0),
                        cursor.getString(1), cursor.getString(2),
                        cursor.getString(3)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return hotels;
    }

}
