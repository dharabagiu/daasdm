package com.pulamea.hotels;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class NewHotelActivity extends AppCompatActivity {

    private HotelsDB hotelsDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_hotel);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        hotelsDB = new HotelsDB(getApplicationContext());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void addHotel(View view) {
        String name = ((EditText)findViewById(R.id.edit_hotel_name)).getText().toString();
        String address = ((EditText)findViewById(R.id.edit_hotel_address)).getText().toString();
        String phone = ((EditText)findViewById(R.id.edit_hotel_phone)).getText().toString();
        String webpage = ((EditText)findViewById(R.id.edit_hotel_webpage)).getText().toString();

        hotelsDB.insertHotel(new Hotel(name, address, phone, webpage));
        finish();
    }
}
