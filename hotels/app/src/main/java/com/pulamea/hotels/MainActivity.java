package com.pulamea.hotels;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private HotelsDB hotelsDB;
    private HotelList hotelsAdapter;
    private List<Hotel> hotels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView hotelsListView = findViewById(R.id.hotel_list);
        /*hotelsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Hotel hotel = hotels.get(position);
                Intent intent = new Intent(Intent.ACTION_DIAL,
                        Uri.fromParts("tel", hotel.getPhone(), null));
                startActivity(intent);
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        hotelsDB = new HotelsDB(getApplicationContext());
        hotels = hotelsDB.getHotels();

        ListView hotelsListView = findViewById(R.id.hotel_list);
        hotelsAdapter = new HotelList(getApplicationContext(), hotels);
        hotelsListView.setAdapter(hotelsAdapter);
    }

    public void newHotel(View view) {
        Intent intent = new Intent(this, NewHotelActivity.class);
        startActivity(intent);
    }

}
