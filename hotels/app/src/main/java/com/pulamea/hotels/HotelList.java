package com.pulamea.hotels;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class HotelList extends ArrayAdapter<Hotel> {

    private static class ViewHolder {
        TextView textName;
        TextView textAddress;
        TextView textPhone;
        TextView textWebpage;
    }

    public HotelList(Context context, List<Hotel> hotels) {
        super(context, R.layout.hotel_list_element, hotels);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Hotel hotel = getItem(position);
        ViewHolder viewHolder;

        View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.hotel_list_element, parent, false);

            viewHolder.textName = convertView.findViewById(R.id.text_name);
            viewHolder.textAddress = convertView.findViewById(R.id.text_address);
            viewHolder.textPhone = convertView.findViewById(R.id.text_phone);
            viewHolder.textWebpage = convertView.findViewById(R.id.text_webpage);

            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
            result = convertView;
        }

        viewHolder.textName.setText(hotel.getName());
        viewHolder.textAddress.setText(hotel.getAddress());
        viewHolder.textAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri intentUri = Uri.parse("geo:0,0?q=" + Uri.encode(((TextView)view).getText().toString()));
                Intent intent = new Intent(Intent.ACTION_VIEW, intentUri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setPackage("com.google.android.apps.maps");
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(intent);
                }
            }
        });
        viewHolder.textPhone.setText(hotel.getPhone());
        viewHolder.textPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = ((TextView)view).getText().toString();
                Intent intent = new Intent(Intent.ACTION_DIAL,
                        Uri.fromParts("tel", phoneNumber, null));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
            }
        });
        viewHolder.textWebpage.setText(hotel.getWebpage());
        viewHolder.textWebpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String webpage = ((TextView)view).getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(webpage));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }
}
