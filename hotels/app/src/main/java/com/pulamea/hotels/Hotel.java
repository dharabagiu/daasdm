package com.pulamea.hotels;

public class Hotel {

    private String name;
    private String address;
    private String phone;
    private String webpage;

    public Hotel(String name, String address, String phone, String webpage) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.webpage = webpage;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebpage() {
        return webpage;
    }
}
